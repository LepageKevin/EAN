def getKey(ean_13)
	sum = 0
	numbers = ean_13.to_s.split('')
	(0..numbers.count).each do |i|
		if i.odd?
			sum += numbers[i].to_i * 3
		else
			sum += numbers[i].to_i
		end
	end

	return sum.modulo(10) if sum.modulo(10) == 0

	return 10 - sum.modulo(10)
end

p getKey(303792016200)