<?php

	function getKey(int $ean13) : int
	{
		$sum = 0;
		$arrayEan13 = str_split($ean13);
		
		for ($i = 0; $i < count($arrayEan13); $i++) {
			if ($i % 2 == 0) {
				$sum += $arrayEan13[$i];
			}
			else {
				$sum += 3 * $arrayEan13[$i];
			}
		}

		$key = $sum % 10;
		if ($key == 0) {
			return $key;
		}
		else {
			return 10 - $key;
		}
	}

	echo getKey(303792016200);

?>